# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

vagrantfiledir=File.expand_path(File.dirname(__FILE__))

saltrepourl=ENV['SALTREPO'] || ''

salttooling=<<-SHELL
apt-get install -y vim git tmux python3-virtualenv python3-dev psmisc build-essential libffi-dev curl pkg-config libssl-dev
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
SHELL

saltclone=<<-SHELL
if [ -z "$1" ]; then
  git clone https://github.com/saltstack/salt
else
  mv /tmp/secure/id_rsa /home/vagrant/.ssh/id_rsa
  cat >>/home/vagrant/.ssh/config <<-SSHCONF
Host gitlab.com
  StrictHostKeyChecking no
  UserKnownHostsFile=/dev/null
SSHCONF
  git clone $1
  cd salt
  git remote add upstream https://github.com/saltstack/salt
  git fetch --tags upstream
  cd ..
fi
SHELL

saltdev=<<-SHELL
mkdir -p /home/vagrant/.local/bin
echo "PATH=$PATH:/home/vagrant/.local/bin" >> /home/vagrant/.bashrc
sudo mkdir /var/log/salt
sudo chown -R vagrant /var/log/salt
# Setup virtualenv and devtools
virtualenv -p python3 venv
source /home/vagrant/venv/bin/activate
pip install --upgrade pip
pip install pyzmq PyYAML pycrypto msgpack jinja2 psutil futures tornado pylint saltpylint

# Setup for document generation
pip install Sphinx==1.3.1

# A fresh install of salt 3006.3 fails on first run
pip install -e ./salt || pip install -e ./salt

# Setup local configuration
mkdir -p ./venv/etc/salt/pki/{master,minion}
cp ./salt/conf/master ./salt/conf/minion ./venv/etc/salt/
sed -i -e 's/^#user:.*/user: vagrant/' -e 's!^#root_dir:.*!root_dir: /home/vagrant/venv!' ./venv/etc/salt/master
sed -i -e 's/^#user:.*/user: vagrant/' -e 's!^#root_dir:.*!root_dir: /home/vagrant/venv!' ./venv/etc/salt/minion
sed -i -e 's/^#master: salt/master: localhost/' -e 's/#id:.*/id: saltdev/' ./venv/etc/salt/minion

# Install salt development
mkdir -p ./venv/srv/{salt,pillar}

# Startup salt
cd /home/vagrant/venv
salt-master -c ./etc/salt -l debug -d
sleep 5
salt-minion -c ./etc/salt -l debug -d
sleep 15
salt-key -c ./etc/salt -L
salt-key -c ./etc/salt -A -y
sleep 10

# Quick sanity check
salt -c ./etc/salt '*' test.version
SHELL

Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider "virtualbox" do |vm|
    vm.name = "salt.dev"
    vm.memory = 8192
    vm.cpus = 2
  end

  if File.exists?("%s/secure" % vagrantfiledir)
    config.vm.provision "file", source: "%s/secure" % vagrantfiledir, destination: "/tmp/secure"
  end

  if File.exist?("%s/files" % vagrantfiledir)
    config.vm.provision "file", source: "%s/files" % vagrantfiledir, destination: "/tmp/files"
  end
  config.vm.provision "shell", inline: salttooling
  config.vm.provision "shell", privileged: false, inline: saltclone, args: saltrepourl
  config.vm.provision "shell", privileged: false, inline: saltdev

  scripts = Dir.glob("%s/scripts/*" % vagrantfiledir).reject {|f| File.directory? f}
  for script in scripts do
    config.vm.provision "shell", privileged: false, path: script
  end
  
end
