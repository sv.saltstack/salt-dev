Salt development environment.
============================

`vagrant` environment to setup up a development environment for `salt` according to guidance in https://docs.saltstack.com/en/latest/topics/development/hacking.html

Usage
=====

To create a development system using `salt` GitHub repository.

```
vagrant up
```

`salt` require all commits to be `gpg` signed, so add your `gpg` signing key (if you supply it in the `secure` directory then simply `gpg import /tmp/secure/mykey.asc`) and then add the `keyid` to your `git` configuration (e.g. `git config --global user.signingkey 0A46826A`)

To use a custom `salt` repository:

```
SALTREPO=< Repo URL > vagrant up
```

If your repository requires an `ssh` certificate, place the `id_rsa` file in `./secure` directory.

To provide your own provision scripts, place them in `./scripts`. Any file in this directory is run as a `shell` script. Scripts are run in directory list order.

Any files in `./files` are copied into `/tmp/files` and are available to the `./scripts` scripts.


Developing
==========

```bash
vagrant ssh
```

Once logged in:

```bash
cd venv
. bin/active
```


